## AlphaPose
Install all packages in requirements.txt. 
Then go to [Alpha Pose](https://github.com/MVIG-SJTU/AlphaPose/tree/master), clone repository in this folder (if repository is cloned to any other location our .py files won't run) 
and follow instructions for installation (to install all essentials for Alpha Pose).

Our code can be used for bluring faces on images and videos. To specify which image or video should be processed go to folder paths and in paths.py input image path or video path.<br/><br/>
To blur faces on images use AlphaposeExample.py.<br/>
To blur faces on videos use edit_video.py.
