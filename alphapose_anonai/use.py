from alphapose.pose_detector import AlphaPoseDetector
from paths.paths import image_folder_input, folder_output
import os

#image_folder_input = "examples/demo/slikesljudi3"
#folder_output = "examples/results/slike3"

#can be called only on image_folder_input, which contains only one picture
detector = AlphaPoseDetector()
ret = detector.detect_face(image_folder_input, folder_output)
print(ret)
