from paths.paths import image_folder_input, folder_output
import subprocess
import time

start_time = time.time()


output_pictures = True


#for images
#image_folder_input = "examples/demo/exercises_286"
#folder_output = "examples/results/exercises_286"

"""
if output_pictures:
    subprocess.run(["./run.sh","--indir",image_folder_input, "--outdir",folder_output,"--vis"])
else:
    subprocess.run(["./run.sh","--indir",image_folder_input, "--outdir",folder_output])
"""

#for videos

video_path = "videos/greys_anatomy1.avi"
folder_output = "videos/video_greys_anatomy1_avi"

if output_pictures:
    subprocess.run(["./run.sh","--video",video_path, "--outdir",folder_output, "--vis", "--format", "cmu"])
else:
    subprocess.run(["./run.sh","--video",video_path, "--outdir",folder_output, "--format", "cmu"])

print("time:", time.time() - start_time)