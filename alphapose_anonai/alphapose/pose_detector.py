import subprocess
import json
import os
import time
import numpy as np
import math
import cv2

class AlphaPoseDetector: #is used in AlphaposeExample
    def __init__(self):
        # body parts for which alphapose gives info
        self.body_parts = ['RAnkle', 'Rknee', 'RHip', 'LHip', 'LKnee', 'LAnkle', \
                      'Pelv', 'Thrx', 'Neck', 'Head', \
                      'RWrist', 'RElbow', 'RShoulder', 'LShoulder', 'LElbow', 'LWrist']

    def read_json_file(self, file_path):
        """
        @brief reads .json file at file_path (which is output of alphapose) and returns a dict with info about location of body parts in the picture
        @result pictures dict: shape od the dict: pictures_dict[img_name][person_number][body_part]
        """
        
        #start_time = time.time()
        with open(file_path, "r") as json_file:  
            data = json.load(json_file)      
            # num_of_pictures = len(data)
            
            # print(data.keys())
            # for key in data.keys():
            #     print(len(data[key]))
            
            body_parts = ['RAnkle', 'Rknee', 'RHip', 'LHip', 'LKnee', 'LAnkle', \
                          'Pelv', 'Thrx', 'Neck', 'Head', \
                          'RWrist', 'RElbow', 'RShoulder', 'LShoulder', 'LElbow', 'LWrist']
            
            #data.keys() - names of images in a folder

            pictures_dict = {key: {i: {body_parts[j//3]: \
                                           (data[key][i]["keypoints"][j], data[key][i]["keypoints"][j+1], data[key][i]["keypoints"][j+2]) \
                                               for j in range(0, len(data[key][0]["keypoints"]), 3)} \
                                                    for i in range(len(data[key]))} for key in data.keys()}
            
        #print("time for reading json file:", time.time() - start_time)
        return pictures_dict
    
    def get_box_coordinates1(self, pictures_dict, img_name, dict_key, height, width):
        """
        @brief calculates coordinates of top left and bottom right corner of rectangular area which contains face of a person.
             For calculation uses proportion of person's distane between sholders and distance between nead and neck to determine 
             if person is turned sidewise or not and adjusts size of rectengular area accordingly.
        @params pictures_dict: holds info about location of body parts in the picture
        @params dict_key: number that indicates a sprecific person on the picture
        @params height, weight: height and weight of the picture
        """
        x_head = pictures_dict[img_name][dict_key]["Head"][0]
        y_head = pictures_dict[img_name][dict_key]["Head"][1]
        
        x_neck = pictures_dict[img_name][dict_key]["Neck"][0]
        y_neck = pictures_dict[img_name][dict_key]["Neck"][1]
        
        x_lshoulder = pictures_dict[img_name][dict_key]["LShoulder"][0]
        x_rshoulder = pictures_dict[img_name][dict_key]["RShoulder"][0]
        
        #if person is not upside down that will be positive
        y_distance_head_neck = y_neck - y_head
        #if person is looking in the camera that will be positive
        x_distance_shoulders = x_lshoulder - x_rshoulder

        #print("y_distance_head_neck:", y_distance_head_neck)
        #print("x_distance_shoulders:", x_distance_shoulders)
        
        w = abs(x_distance_shoulders)
        h = abs(y_distance_head_neck)

        #if person is turned to the side
        if w < h:
            w = h
            distance_to_the_side = w/2
        else:
            distance_to_the_side = w/4
            
        if y_neck > y_head:
            # if person is not upside down
            if x_neck > x_head:
                top_left_x = x_head - distance_to_the_side
                top_left_y = y_head
                
                bottom_right_x = x_neck + distance_to_the_side
                bottom_right_y = y_neck
            else:
                top_left_x = x_neck - distance_to_the_side
                top_left_y = y_head
                
                bottom_right_x = x_head + distance_to_the_side
                bottom_right_y = y_neck
        else:
            if x_neck > x_head:
                top_left_x = x_head - distance_to_the_side
                top_left_y = y_neck
                
                bottom_right_x = x_neck + distance_to_the_side
                bottom_right_y = y_head
            else:
                top_left_x = x_neck - distance_to_the_side
                top_left_y = y_neck
                
                bottom_right_x = x_head + distance_to_the_side
                bottom_right_y = y_head
                
        return top_left_x, top_left_y, bottom_right_x, bottom_right_y
    
    def get_box_coordinates2(self, pictures_dict, img_name, dict_key, height, width):
        """
        @brief calculates coordinates of top left and bottom right corner of rectangular area which contains face of a person.
            For calculation uses angle of a vector going from persons neck to its head.
        @params pictures_dict: holds info about location of body parts in the picture
        @params dict_key: number that indicates a sprecific person on the picture
        @params height, weight: height and weight of the picture
        """
        x_head = pictures_dict[img_name][dict_key]["Head"][0]
        y_head = pictures_dict[img_name][dict_key]["Head"][1]
        
        x_neck = pictures_dict[img_name][dict_key]["Neck"][0]
        y_neck = pictures_dict[img_name][dict_key]["Neck"][1]
        
        x_distance_head_neck = x_head - x_neck
        y_distance_head_neck = y_neck - y_head
        
        angle = math.degrees(math.atan2(abs(y_distance_head_neck), abs(x_distance_head_neck)))
        print("angle:", angle)
        
        # if angle is close to 0 (person's head is positioned horizontally)
        # rectangle needs to be strached just in y direction, so factor_y is low and factor_x is high
        
        # if angle is close to 90 (person's head is positioned vertically)
        # rectangle needs to be strached just in x direction, so factor_x is low and factor_y is high
        
        if angle <= 23: #person's head is positioned horizontally
            factor_y = 1.3
            factor_x = 10
        elif 23 < angle < 30:
            factor_y = 1.5
            factor_x = 3
        elif 30 <= angle <= 45:
            factor_y = 1.8
            factor_x = 4
        elif 45 < angle < 68:
            factor_y = 4
            factor_x = 1.8
        elif 68 < angle < 75:
            factor_y = 3
            factor_x = 1.5
        elif angle >= 75: #person's head is positioned vertically
            factor_y = 10
            factor_x = 1.3

        # size of blurred rectangle is adjusted with image dimensions
        proportions = True
        
        x_lshoulder = pictures_dict[img_name][dict_key]["LShoulder"][0]
        x_rshoulder = pictures_dict[img_name][dict_key]["RShoulder"][0]
        x_distance_shoulders = x_lshoulder - x_rshoulder
        
        #below if sentance added, otherwise faces of people in position examples/demo/exercises/exercise12.jpg won't be blurred
        if 1.5*abs(x_distance_head_neck) < abs(x_distance_shoulders) and 1.5*abs(y_distance_head_neck) < abs(x_distance_shoulders):
            print("shoulders are wider than length of head neck distance")
            print(x_distance_shoulders)
            print(y_neck)
            x_distance_head_neck = x_distance_shoulders
            y_distance_head_neck = x_distance_shoulders
            factor_x, factor_y = 1.5, 1.5
        
        #determine coordinated of the rectengular area
        if y_neck > y_head:
            #if person is not updside down - upper point is head, bottom is neck
            if proportions and width > height:
                    top_left_y = y_head - height/width*abs(x_distance_head_neck)/factor_y
                    bottom_right_y = y_neck + height/width*abs(x_distance_head_neck)/factor_y
            else:
                top_left_y = y_head - abs(x_distance_head_neck)/factor_y
                bottom_right_y = y_neck + abs(x_distance_head_neck)/factor_y
                
            #print("bottom right y:",bottom_right_y)
            
            if x_neck > x_head:
                #left point is head, right point is neck
                if proportions and height > width:
                    top_left_x = x_head - width/height*abs(y_distance_head_neck)/factor_x
                    bottom_right_x = x_neck + width/height*abs(y_distance_head_neck)/factor_x
                else:
                    top_left_x = x_head - abs(y_distance_head_neck)/factor_x
                    bottom_right_x = x_neck + abs(y_distance_head_neck)/factor_x
            else:
                #left point is neck, right point is nhead
                if proportions and height > width:
                    top_left_x = x_neck - width/height*abs(y_distance_head_neck)/factor_x
                    bottom_right_x = x_head + width/height*abs(y_distance_head_neck)/factor_x
                else:
                    top_left_x = x_neck - abs(y_distance_head_neck)/factor_x
                    bottom_right_x = x_head + abs(y_distance_head_neck)/factor_x
                
        else:
            #if person is updside down - upper point is neck, bottom is head
            if proportions and width > height:
                top_left_y = y_neck - height/width*abs(x_distance_head_neck)/factor_y
                bottom_right_y = y_head + height/width*abs(x_distance_head_neck)/factor_y
            else:
                top_left_y = y_neck - abs(x_distance_head_neck)/factor_y
                bottom_right_y = y_head + abs(x_distance_head_neck)/factor_y
            
            if x_neck > x_head:
                #left point is head, right point is neck
                if proportions and height > width:
                    top_left_x = x_head - width/height*abs(y_distance_head_neck)/factor_x
                    bottom_right_x = x_neck + width/height*abs(y_distance_head_neck)/factor_x
                else:
                    top_left_x = x_head - abs(y_distance_head_neck)/factor_x
                    bottom_right_x = x_neck + abs(y_distance_head_neck)/factor_x
            else:
                #left point is neck, right point is nhead
                if proportions and height > width:
                    top_left_x = x_neck - width/height*abs(y_distance_head_neck)/factor_x
                    bottom_right_x = x_head + width/height*abs(y_distance_head_neck)/factor_x
                else:
                    top_left_x = x_neck - abs(y_distance_head_neck)/factor_x
                    bottom_right_x = x_head + abs(y_distance_head_neck)/factor_x

        
        return top_left_x, top_left_y, bottom_right_x, bottom_right_y
        
        
        
    def get_coordinates(self, image_folder_input, folder_output, img_name = ""):
        """
        @brief calculates coordinates of top left and bottom right corners of rectangular areas which contain faces of people
              in the picture which is saved in image_folder_input.
              If img_name is not given it is assumed that only one picture is in that folder.
              Function returns array of subarrays that contains that info for each person in the picture or None if there is no person detected.
        @params image_folder_input: path to the folder where input picture is saved
        @params folder_output: path to the folder where results will be created
        @params height, weight: height and weight of the picture
        """
        if len(os.listdir(folder_output)) == 0:
            #if folder with results is empty from before and on current image no figure is detected
            #folder POSE and .json file are not generated
            return None
        
        elif os.listdir("/".join([folder_output, "POSE"])) == []:
            #if folder POSE is empty from before and on current image no figure is detected
            #then .json file is not generated
            return None

        
        pictures_dict = self.read_json_file("/".join([folder_output, "POSE", "alpha-pose-results-forvis.json"]))
        
        #print(pictures_dict)
        if not bool(pictures_dict):
            return None #there is no face on the picture
        
        else:
            if img_name == "":
                #if only one picture is in the folder
                #img_name = os.listdir("/".join(["..", image_folder_input]))[0] #not ok?
                img_name = os.listdir(image_folder_input)[0]

            #get height and width of the picture
            img_path = "/".join([image_folder_input, img_name])
            #print(img_path)
        
            img = cv2.imread(img_path)
            height, width, _ = img.shape
            
            #print("image folder input:", image_folder_input)
            if "results" in image_folder_input:
                # pictures in folder results (alphapose output) are all in .png format regardless of input format
                if img_name.split(".")[1] == "png":
                    img_name = img_name.split(".")[0] + ".jpg"
                
            if img_name not in pictures_dict:
                #no figure is detected on this picture so .json file doesn't contain img_name
                return None

            boxes = []
            
            print("\nimg name:", img_name)
            for dict_key in pictures_dict[img_name]: #for each person
                    top_left_x, top_left_y, bottom_right_x, bottom_right_y = self.get_box_coordinates2(pictures_dict, img_name, dict_key, height, width)
                    boxes.append([top_left_x, top_left_y, bottom_right_x, bottom_right_y])
                    
            return boxes
         
        
    
    def detect_face(self, image_folder_input, folder_output, output_pictures = False):
        """
        @brief detects figures in all pictures in image_folder_input and create results in folder_output.
              Returs array with one subarray for each person where coordinates of top left and bottom right corners
              of rectangular area which contains face are stored. If no figure is detected returns None.
        @params image_folder_input: path to the folder where input picture is saved
        @params folder_output: path to the folder where results will be created
        @params output pictures: If True alpahpose will generate output pictures with pose detection drown on them
        """

        start_time = time.time()
        
        if output_pictures:
            subprocess.run(["./run.sh","--indir",image_folder_input, "--outdir",folder_output,"--vis"])
        else:
            subprocess.run(["./run.sh","--indir",image_folder_input, "--outdir",folder_output])
        
        
        print("time for alphapose model:", time.time() - start_time)

        # ret is array of subarrays or None
        ret = self.get_coordinates(image_folder_input, folder_output)
        print(ret)
        
        return ret

    def process_images(self, image_folder_input, folder_output, output_pictures = False): 
        """
        @ brief process images in image_folder_input and generete results in folder_output
        @params image_folder_input: path to the folder where input picture is saved
        @params folder_output: path to the folder where results will be created
        @params output pictures: If True alpahpose will generate output pictures with pose detection drown on them
        """
        start_time = time.time()
        
        if output_pictures:
            subprocess.run(["./run.sh","--indir",image_folder_input, "--outdir",folder_output,"--vis"])
        else:
            subprocess.run(["./run.sh","--indir",image_folder_input, "--outdir",folder_output])
        
        print("time for alphapose model:", time.time() - start_time)

        
