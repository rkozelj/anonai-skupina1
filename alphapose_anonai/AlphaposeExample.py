from alphapose.pose_detector import AlphaPoseDetector
from paths.paths import image_folder_input, folder_output

import os
import cv2
from math import sqrt
import time

# result codes for methods
RESULT_OK = 0
RESULT_GENERAL_ERROR = -1
RESULT_NO_FACE_DETECTED = -2

class Detector:
    def __init__(self, image_size=(112, 112), no_face_raise = True):
        self.no_face_raise = no_face_raise
        self.model = FaceModel()

    def detect(self, image_folder_input, folder_output, output_pictures = False, method = "frame by frame", img_name = ""):
        """
        @brief performs face detection on input image saved in image_folder_input
        @params folder_output: alphapose results location
        @params output pictures: If True alpahpose will generate output pictures with pose detection drown on them
        @params method:  parameter for editing a video, if "frame by frame" picture wasn't processed by alphapose jet, 
                        if "all frames" it already was and only coordinates of rectangular area need to be calculated
        @retval: a tuple containing result code and the image
        """
        result, blurred_image, face_boxes = self.model.get_input(image_folder_input, folder_output, output_pictures, method, img_name)

        if img_name == "":
            img_name = os.listdir(image_folder_input)[0]
            
        image_path = "/".join([image_folder_input, img_name])
        #print("joining:", img_name)
        image = cv2.imread(image_path)

        if result != RESULT_OK:
            if self.no_face_raise:
                return RESULT_NO_FACE_DETECTED, image, None
            else:
                return RESULT_GENERAL_ERROR, image, None
        else:
            return RESULT_OK, blurred_image, face_boxes


class FaceModel:
    def __init__(self):
        detector = AlphaPoseDetector()
        self.detector = detector

    def get_input(self, image_folder_input, folder_output, output_pictures = False, method = "frame by frame", img_name = ""):
        """
        @brief performs face detection on input image saved in image_folder_input
        @params folder_output: alphapose results location
        @params output pictures: If True alpahpose will generate output pictures with pose detection drown on them
        @params method: parameter for editing a video, if "frame by frame" picture wasn't processed by alphapose jet, 
                        if "all frames" it already was and only coordinates of rectangular area need to be calculated
        @retval: a tuple containing result code and the image
        """
        
        if method == "frame by frame":
            ret = self.detector.detect_face(image_folder_input, folder_output, output_pictures)
        elif method == "all frames":
            #.json file is already generated
            ret = self.detector.get_coordinates(image_folder_input, folder_output, img_name)
        #print("coordinates:", ret)

        if img_name == "":
            img_name = os.listdir(image_folder_input)[0]
        
        image_path = "/".join([image_folder_input, img_name])
        face_img = cv2.imread(image_path)
        
        if ret is None:
            return RESULT_NO_FACE_DETECTED, None, None
            
        blur_image = face_img.copy()
    
        face_boxes = []
        boxes = ret
        for i in range(0, len(boxes)):
            rec_x = int(boxes[i][0]) #x: top left corner of the rectangular area
            rec_y = int(boxes[i][1]) #x: top left corner of the rectangular area
            rec_width = int(boxes[i][2] - boxes[i][0]) #x (bottom right corner) - x (top left corner)
            rec_height = int(boxes[i][3] - boxes[i][1]) #y (bottom right corner) - y (top left corner)
            blur_image = blur_face(blur_image, rec_x, rec_y, rec_width, rec_height)
            face_boxes.append([rec_x, rec_y, rec_width, rec_height])
            
        #draw bottom right and upper left points of rectengular area
        #for i in range(0, len(boxes)):
        #    blur_image = draw_points(blur_image, [int(boxes[i][0]), int(boxes[i][1]), int(boxes[i][2]), int(boxes[i][3])])
            
        #print(RESULT_OK, face_boxes)
        return RESULT_OK, blur_image, face_boxes


def adjust_window_size(image, title):
    """
    @bref resize image so that the longer coordinate is 1000
    """
    cv_size = lambda img: tuple(img.shape[1::-1])
    #print(cv_size(image))
    
    cv2.namedWindow(title, cv2.WINDOW_NORMAL)
    #cv2.resizeWindow('title', 600, 600.5)
                     
    width, height = cv_size(image)
    #print(width, height)
    
    if width > height:
        cv2.resizeWindow(title, 1000, int(height/width*1000))
    else:
        cv2.resizeWindow(title, int(width/height*1000, 1000))

        
def detect_face_on_image(image_folder_input, folder_output, output_pictures = False, method = "frame by frame", img_name = ""):
    """
    @brief detects faces on image in image_folder_input and blurs them
    @params folder_output: alphapose results location
    @params output pictures: If True alpahpose will generate output pictures with pose detection drown on them
    @params method: parameter for editing a video, if "frame by frame" picture wasn't processed by alphapose jet, 
                    if "all frames" it already was and only coordinates of rectangular area need to be calculated
    @retval image with blurred faces
    """
    detector = detection_model_init()
    #image_path = "/".join([image_folder_input, image_name]) 
    #img_name = cv2.imread(image_path)
    res, blur_image, face_boxes = detector.detect(image_folder_input, folder_output, output_pictures, method, img_name)

    cv2.imshow("title", blur_image)
    cv2.waitKey(0)
    cv2.destroyAllWindows()

    #save image
    #img_folder = image_folder_input
    #img_name = os.listdir(img_folder)[0]
    #cv2.imwrite("/".join([img_folder, img_name.split(".")[0] + "_result_alphapose.jpg"]),blur_image)


def detect_face_video_frame(detector, image_folder_input, folder_output, output_pictures = False, method = "frame by frame", img_name = ""):
    """
    @brief detects faces on video frames saved in image_folder_input and blurs them
    @params folder_output: alphapose results location
    @params output pictures: If True alpahpose will generate output pictures with pose detection drown on them
    @params method: parameter for editing a video, if "frame by frame" picture wasn't processed by alphapose jet, 
                    if "all frames" it already was and only coordinates of rectangular area need to be calculated
    @retval video framed with blurred faces
    """
    res, frame_blurred, face_boxes = detector.detect(image_folder_input, folder_output, output_pictures, method, img_name)
    return res, frame_blurred, face_boxes


def detection_model_init():
    """
    @brief initialisation of detector for detecting faces
    """
    detector = Detector()
    return detector

def draw_points(face_img, points):
    """
    @brief draw points on face_img
    @param face_img: input image encoded as numpy array
    @param points: array with coordinates of top left corner and bottom right corner of rectengular area
    """
    font = cv2.FONT_HERSHEY_SIMPLEX
    face_img = cv2.circle(face_img,(points[0], points[1]), 3, (255, 0,0), -1)
    face_img = cv2.putText(face_img, "top left",(points[0], points[1]), font, 0.5, (255,255,255), 2, cv2.LINE_AA)
    face_img = cv2.circle(face_img,(points[2], points[3]), 3, (255, 0,0), -1)
    face_img = cv2.putText(face_img, "bottom right",(points[2], points[3]), font, 0.5, (255,255,255), 2, cv2.LINE_AA)
    
    #cv2.imshow("title", face_img)
    #cv2.waitKey(0)
    #cv2.destroyAllWindows()
    
    return face_img


def blur_face(face_img, x, y, w, h, filter_coeff=0.25, sigma_coeff=0.75):
    """
    @brief blur the rectangular area in the image
    @param face_img: input image encoded as numpy array
    @param x, y: top left corner of the rectangular area
    @param w, h: width and height of the rectangular area
    """
    if w<0 or h<0:
        print('blur_face: invalid box input dimensions')
        return face_img

    if x > face_img.shape[1] or y > face_img.shape[0]:
        return face_img

    if x<0:
        x = 0

    if y<0:
        y = 0

    blur_image = face_img
    cv2.rectangle(blur_image, (x, y), (x+w, y+h), (255,255,0), thickness=0)
    sub_face = blur_image[y:y+h, x:x+w]

    # set filter according to rectangle surface area
    # filter size must be an odd number
    face_surface = w*h
    filter_size = round(filter_coeff*sqrt(face_surface)/2)*2+1
    filter_sigma = filter_size/sigma_coeff

    #print('face surface: ', w*h)
    #print('filter parameters: ', filter_size, filter_sigma)
    sub_face = cv2.GaussianBlur(sub_face, (filter_size,filter_size), filter_sigma)
    blur_image[y:y+sub_face.shape[0], x:x+sub_face.shape[1]] = sub_face
    
    return blur_image

if __name__ == "__main__":
  start_time = time.time()
  detect_face_on_image(image_folder_input, folder_output)
  print("time for image:", time.time() - start_time)
