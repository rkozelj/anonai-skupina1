# -*- coding: utf-8 -*-
"""
Created on Tue Jul  9 10:50:50 2019

@author: Romi
"""
import json
import numpy as np
from math import sqrt
import cv2
import os
import shutil
from alphapose.pose_detector import AlphaPoseDetector

def read_json_file(file_path):
    """
    @brief reads .json file at file_path (which is output of alphapose) and returns a dict with info about location of body parts in the picture
    @result pictures dict: shape od the dict: pictures_dict[img_name][person_number][body_part]
    """
    with open(file_path, "r") as json_file:  
        data = json.load(json_file)      
        # num_of_pictures = len(data)
        
        # print(data.keys())
        # for key in data.keys():
        #     print(len(data[key]))
        
        body_parts = ['RAnkle', 'Rknee', 'RHip', 'LHip', 'LKnee', 'LAnkle', \
                      'Pelv', 'Thrx', 'Neck', 'Head', \
                      'RWrist', 'RElbow', 'RShoulder', 'LShoulder', 'LElbow', 'LWrist']
        
        #data.keys() - names of images in a folder

        pictures_dict = {key: {i: {body_parts[j//3]: \
                                       (data[key][i]["keypoints"][j], data[key][i]["keypoints"][j+1], data[key][i]["keypoints"][j+2]) \
                                           for j in range(0, len(data[key][0]["keypoints"]), 3)} \
                                                for i in range(len(data[key]))} for key in data.keys()}
        
    return pictures_dict
    
def draw_on_picture(image_folder_input, folder_output, img_path, pictures_dict, points_for_parts = False, points_for_one_person = False, points_for_all = False, blur_all_faces = True):
    """
    @brief draws points of body parts and blur faces in the picture at img_path
    @params image_folder_input, folder_output: alphapose parameters
    @params pictures_dict: holds info about location of body parts in the picture
    @params points_for_parts: array of body parts that will be drawn on the picture or False
    @params points_for_one_person: number (in a string) of a person whose points will be drawn or False
    @params points_for_all: if True all body_parts of all the people will be drawn
    @params blur_all_faces: if True all faces in the picture will be blurred
    """
    body_parts = ['RAnkle', 'Rknee', 'RHip', 'LHip', 'LKnee', 'LAnkle', \
                      'Pelv', 'Thrx', 'Neck', 'Head', \
                      'RWrist', 'RElbow', 'RShoulder', 'LShoulder', 'LElbow', 'LWrist']
    
    img_folder = img_path.split("/")[:-1]
    img_folder = "/".join(img_folder)
    #print(img_folder)
    img_name = img_path.split("/")[-1]
    
    img = cv2.imread(img_path) #convert picture to array
    #print(img)
    #cv2.imshow("title", img)
    
    #*************** draw blurred squares on faces *********************
    blur_image = img.copy()
    
    if blur_all_faces:
        #specify detector
        detector = AlphaPoseDetector()
        #print(detector)
        
        ret = detector.get_coordinates(image_folder_input, folder_output, img_name)
        #ret = detector.detect_face(image_folder_input, folder_output, output_pictures = True)
        
        if ret is not None:
            boxes = ret
            #print(boxes)

            face_boxes = []
            for i in range(0, len(boxes)):
                rec_x = int(boxes[i][0]) #x: top left corner of the rectangular area
                rec_y = int(boxes[i][1]) #x: top left corner of the rectangular area
                rec_width = int(boxes[i][2] - boxes[i][0]) #x (bottom right corner) - x (top left corner)
                rec_height = int(boxes[i][3] - boxes[i][1]) #y (bottom right corner) - y (top left corner)
                #print("rec_width:", rec_width)
                #print("rec_height:", rec_height)
                blur_image = blur_face(blur_image, rec_x, rec_y, rec_width, rec_height)
                face_boxes.append([rec_x, rec_y, rec_width, rec_height])

            #draw bottom right and upper left points of rectengular area
            #for i in range(0, len(boxes)):
            #    blur_image = draw_points(blur_image, [int(boxes[i][0]), int(boxes[i][1]), int(boxes[i][2]), int(boxes[i][3])])

            #adjust_window_size(blur_image, "title")

            #show picture 
            cv2.imshow("title", blur_image)
            cv2.waitKey(0)
            #cv2.destroyAllWindows()

            #save picture
            #img_folder = "/".join(img_path.split("/")[:-1])
            #img_folder = "../insightface_alphapose_comparison/images"
            #cv2.imwrite("/".join([img_folder, img_name.split(".")[0] + "_alphapose.jpg"]),blur_image)
            
    #************ draw just one body part ***************
    if "results" in image_folder_input:
        # pictures in folder results (alphapose output) are all in .png format regardless of input format
        img_name = img_name.split(".")[0] + ".jpg"
        
    img = blur_image.copy()
    
    if points_for_parts:
        for body_part in points_for_parts:
            body_part_data = [pictures_dict[img_name][dict_key][body_part] for dict_key in pictures_dict[img_name]]
            body_part_pts = np.array([[body_part_data[i][0], body_part_data[i][1]] for i in range(0, len(body_part_data))], np.int32)
            
            #draw body part name on the picture
            for i in range(0, len(body_part_pts)):
                cv2.circle(img, (body_part_pts[i][0], body_part_pts[i][1]), 3, (0, 255,0), -1)
                font = cv2.FONT_HERSHEY_SIMPLEX
                cv2.putText(img, body_part + " " + str(round(body_part_data[i][2], 2)) ,(body_part_pts[i][0], body_part_pts[i][1]), font, 0.5, (255,255,255), 2, cv2.LINE_AA)

        #show picture     
        cv2.imshow("title", img)
        cv2.waitKey(0)
        cv2.destroyAllWindows()
    
    # ************* draw just for one person ****************
    if points_for_one_person:
        dict_key = int(points_for_one_person) #person
        for body_part in body_parts:
            body_part_data = [pictures_dict[img_name][dict_key][body_part]]
            body_part_pts = np.array([[body_part_data[i][0], body_part_data[i][1]] for i in range(0, len(body_part_data))], np.int32)

            #draw body part name on the picture
            for i in range(0, len(body_part_pts)):
                cv2.circle(img, (body_part_pts[i][0], body_part_pts[i][1]), 5, (0, 255,0), -1)
                font = cv2.FONT_HERSHEY_SIMPLEX
                cv2.putText(img,body_part + " " + str(round(body_part_data[i][2], 2)) ,(body_part_pts[i][0], body_part_pts[i][1]), font, 0.5, (255,255,255), 2, cv2.LINE_AA)

        #show picture       
        cv2.imshow("title", img)
        cv2.waitKey(0)
        cv2.destroyAllWindows()
            
    #************ draw everything **************************
    if points_for_all:
        for body_part in body_parts:
        
            body_part_data = [pictures_dict[img_name][dict_key][body_part] for dict_key in pictures_dict[img_name]]
            body_part_pts = np.array([[body_part_data[i][0], body_part_data[i][1]] for i in range(0, len(body_part_data))], np.int32)

            #draw body part name on the picture
            for i in range(0, len(body_part_pts)):
                cv2.circle(img, (body_part_pts[i][0], body_part_pts[i][1]), 5, (0, 255,0), -1)
                font = cv2.FONT_HERSHEY_SIMPLEX
                cv2.putText(img,body_part + " " + str(round(body_part_data[i][2], 2)) ,(body_part_pts[i][0], body_part_pts[i][1]), font, 0.5, (255,255,255), 2, cv2.LINE_AA)

        #show picture
        cv2.imshow("title", img)
        cv2.waitKey(0)
        cv2.destroyAllWindows()
    


def adjust_window_size(image, title):
    """
    @bref resize image so that the longer coordinate is 1000
    """
    cv_size = lambda img: tuple(img.shape[1::-1])
    #print(cv_size(image))
    
    cv2.namedWindow(title, cv2.WINDOW_NORMAL)
    #cv2.resizeWindow('title', 600, 600.5)
                     
    width, height = cv_size(image)
    #print(width, height)
    
    if width > height:
        cv2.resizeWindow(title, 1000, int(height/width*1000))
    else:
        cv2.resizeWindow(title, int(width/height*1000, 1000))
        
def draw_points(face_img, points):
    """
    @brief draw points on face_img
    @param face_img: input image encoded as numpy array
    @param points: array with coordinates of top left corner and bottom right corner of rectengular area
    """
    font = cv2.FONT_HERSHEY_SIMPLEX
    face_img = cv2.circle(face_img,(points[0], points[1]), 3, (255, 0,0), -1)
    face_img = cv2.putText(face_img, "top left",(points[0], points[1]), font, 0.5, (255,255,255), 2, cv2.LINE_AA)
    face_img = cv2.circle(face_img,(points[2], points[3]), 3, (255, 0,0), -1)
    face_img = cv2.putText(face_img, "bottom right",(points[2], points[3]), font, 0.5, (255,255,255), 2, cv2.LINE_AA)
    
    #cv2.imshow("title", face_img)
    #cv2.waitKey(0)
    #cv2.destroyAllWindows()
    
    return face_img

def blur_face(face_img, x, y, w, h, filter_coeff=0.25, sigma_coeff=0.75):
    """
    @brief blur the rectangular area in the image
    @param face_img: input image encoded as numpy array
    @param x, y: top left corner of the rectangular area
    @param w, h: width and height of the rectangular area
    """
    if w<0 or h<0:
        print('blur_face: invalid box input dimensions')
        return face_img

    if x > face_img.shape[1] or y > face_img.shape[0]:
        return face_img

    if x<0:
        x = 0

    if y<0:
        y = 0

    blur_image = face_img
    cv2.rectangle(blur_image, (x, y), (x+w, y+h), (255,255,0), thickness=0)
    sub_face = blur_image[y:y+h, x:x+w]

    # set filter according to rectangle surface area
    # filter size must be an odd number
    face_surface = w*h
    filter_size = round(filter_coeff*sqrt(face_surface)/2)*2+1
    filter_sigma = filter_size/sigma_coeff

    #print('face surface: ', w*h)
    #print('filter parameters: ', filter_size, filter_sigma)
    sub_face = cv2.GaussianBlur(sub_face, (filter_size,filter_size), filter_sigma)
    blur_image[y:y+sub_face.shape[0], x:x+sub_face.shape[1]] = sub_face
    
    return blur_image

def delete_folder(path):
    """
    @brief deletes folder at path
    """
    shutil.rmtree(path)
    
def delete_files_in_directory(path):
    """
    @brief deletes all files and folders in directory at path
    """
    files = os.listdir(path)
    print(files)
    for file in files:
        try:
            os.remove("/".join([path, file])) #if it is not a folder
        except:
            shutil.rmtree("/".join([path, file])) #in case it is a folder

if __name__ == "__main__":
    #test coordinated for on eimage
    #image_folder_input = "examples1/results"
    #file_path = "examples1/results/POSE/alpha-pose-results-forvis.json"
    #folder_output = "examples1/results"
    #pictures_dict = read_json_file(file_path)
    #img_path = "examples1/results/RENDER/people5.png"
    #draw_on_picture(image_folder_input, folder_output, img_path, pictures_dict, points_for_parts = False, points_for_one_person = "", points_for_all = False, blur_all_faces = True)

    
    #test coodrinates for one image
    """
    img_path = "examples/results/exercises/RENDER/exercise4.png"
    folder_output = "examples/results/exercises"
    image_folder_input = "examples/results/exercises/RENDER"
    file_path = "/".join([folder_output, "POSE", "alpha-pose-results-forvis.json"])
    pictures_dict = read_json_file(file_path)
    draw_on_picture(image_folder_input, folder_output, img_path, pictures_dict, points_for_parts = False, points_for_one_person = "1", points_for_all = False, blur_all_faces = True)
    """
  
    #test coordinates for all images in image_folder_input
    
    image_folder_input = "examples/results/exercises/RENDER/"
    folder_output = "examples/results/exercises"
    file_path = "/".join([folder_output, "POSE", "alpha-pose-results-forvis.json"])
    pictures_dict = read_json_file(file_path)
    
    names = os.listdir(image_folder_input)
    names = sorted(names)
    
    #for i in range(1, 20):
    for img_name in names:
        #img_name = "exercise{0}.png".format(i)
        img_path = "/".join([image_folder_input, img_name])
        draw_on_picture(image_folder_input, folder_output, img_path, pictures_dict, points_for_parts = ["Head", "Neck", "RShoulder", "LShoulder"], points_for_one_person = "", points_for_all = False, blur_all_faces = True)

    
    path = "unneeded/results"
    #delete_folder(path)
    #delete_files_in_directory(path)


    #tests for pictures in folder exercises
    """
    image_folder_input = "examples/results/exercises"
    folder_output = "examples/results/exercises"

    detector = AlphaPoseDetector()
    detector.process_images(image_folder_input, folder_output)
    
    image_folder_input = "examples/results/exercises/RENDER"
    images = os.listdir(image_folder_input)
    for image_name in images:
        img_path = "/".join([image_folder_input, image_name])
        draw_on_picture(image_folder_input, folder_output, img_path, dict(), points_for_parts = False, points_for_one_person = "", points_for_all = False, blur_all_faces = True)
    """
