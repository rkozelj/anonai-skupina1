import cv2
import time
import FaceBlurUtils as utls
import os
import shutil

from paths.paths import video_path
from FaceDetector import FaceDetector as FaceDetector
from alphapose.pose_detector import AlphaPoseDetector

def delete_files_in_directory(path):
    """
    @bref delete files in directory at path
    """
    files = os.listdir(path)
    print("deleting:", files)
    
    for file in files:
        try:
            os.remove("/".join([path, file])) #if it is not a folder
        except:
            shutil.rmtree("/".join([path, file])) #in case it is a folder


def process_video_frame_by_frame(video_path, video_path_output, image_folder_input, folder_output, output_pictures, detector):
    """
    @brief blur faces in video at video_path and returns blurred video at video_path_output.
            Function process video frames one by one with alphapose and uses generated data to detect faces 
            on those frames and gather them in an outputed video.
    @params image_folder_input, folder_output: alphapose parameters
    @params output pictures: If True alpahpose will generate output pictures with pose detection drown on them
    @param detector: detector used for detecting faces
    """
    print("opening: ", video_path)
    input_video = cv2.VideoCapture(video_path)
    print("Input video:", input_video)

    #default video dimensions
    video_xdim = 800
    video_ydim = 600

    if input_video.isOpened():
        video_xdim = int(input_video.get(3))
        video_ydim = int(input_video.get(4))
        print('video dimensions: ', video_xdim, 'x', video_ydim)
    else:
        print("failed to open video file")
        exit(-1)

    # specify video encoding and format
    fourcc = cv2.VideoWriter_fourcc(*'XVID')
    
    output_video = cv2.VideoWriter(video_path_output, fourcc, 20.0, (video_xdim, video_ydim))
    print("output to: ", video_path_output)
    cntr = 0
    
    while(input_video.isOpened()):  
        cntr = cntr + 1
        print("\nframe: ", cntr)
        # ret is False if there are no more frames to read
        
        ret, frame = input_video.read()
        #print("ret, frame:", ret, frame)
        
        if ret == True:
            
            img_name = str(cntr) + ".jpg"
            cv2.imwrite("/".join([image_folder_input, img_name]), frame)

            # do stuff with frame
            res, result_container = detector.face_blur(image_folder_input, folder_output, output_pictures = output_pictures)
            print("res:", "result frame" if res == 1 else "result queue" if res == 2 else "None")
            print("result container:", len(result_container))
    ##        if res == detector.RESULT_FRAME:
    ##            cv2.imshow("title", result_container)
    ##            cv2.waitKey(0)
    ##            cv2.destroyAllWindows()

            # delete image
            delete_files_in_directory(image_folder_input)

            # load processed frame into video
            if res == detector.RESULT_FRAME:
                output_video.write(result_container)
            
            if res == detector.RESULT_QUEUE:
                # print("interpolating: ", detector.face_coordinates_falling, detector.face_coordinates_rising, "delta: ", detector.face_coordinates_delta)
                print("returned cache size ", len(result_container))
                for i in range(0, len(result_container)):
                    output_video.write(result_container.pop())
        else:
            # empty the current detector cache
            for i in range(0, len(detector.frame_cache)):
                output_video.write(detector.frame_cache.pop())
                
            print("write done")
            break

    ##    if res == detector.RESULT_FRAME:
    ##        cv2.imshow("title", frame)
    ##        cv2.waitKey(0)
    ##        cv2.destroyAllWindows()

    input_video.release()
    output_video.release()
    cv2.destroyAllWindows()
    
    return output_video



def process_video_all_frames(video_path, video_path_output, image_folder_input, folder_output, output_pictures, detector, processor):
    """
    @brief blur faces in video at video_path and returns blurred video at video_path_output.
           Function first saves all frames that video consists of in a folder, then process them all at once with alphapose and then
           use alphapose data to detect faces on those frames and gather them in an outputed video.
    @params image_folder_input, folder_output: alphapose parameters
    @params output pictures: If True alpahpose will generate output pictures with pose detection drown on them
    @param detector: detector used for detecting faces
    @param processor: processor used for procesing images
    """
    print("opening: ", video_path)
    input_video = cv2.VideoCapture(video_path)
    print("Input video:", input_video)

    #default video dimensions
    video_xdim = 800
    video_ydim = 600

    if input_video.isOpened():
        video_xdim = int(input_video.get(3))
        video_ydim = int(input_video.get(4))
        print('video dimensions: ', video_xdim, 'x', video_ydim)
    else:
        print("failed to open video file")
        exit(-1)

    # specify video encoding and format
    fourcc = cv2.VideoWriter_fourcc(*'XVID')
    
    output_video = cv2.VideoWriter(video_path_output, fourcc, 20.0, (video_xdim, video_ydim))
    print("output to: ", video_path_output)
    cntr = 0
    
    rets = []
    img_names = []

    # save all images that video consist of in image_folder_input
    while(input_video.isOpened()):
        cntr = cntr + 1
        print("\nframe: ", cntr)
        # ret is False if there are no more frames to read
        
        ret, frame = input_video.read()
        rets.append(ret)
        #print("ret, frame:", ret, frame)
        if ret == True:
            img_name = str(cntr) + ".jpg"
            img_names.append(img_name)
            
            cv2.imwrite("/".join([image_folder_input, img_name]), frame)
        else:
            break
        
    
    #if pictures were already processed by alphapose, command below is not needed 
    #comment below command when adjusting function for coordinates

    #process all pictures in image_folder_input and generate results in folder_output
    processor.process_images(image_folder_input, folder_output, output_pictures)
    
    #create output video from alphapose output pictures
#    image_folder_input_results = "/".join([folder_output, "RENDER"])
#    image_folder_input_real = image_folder_input
    
    # on all processed images detect figures, blur faces and format them into output video
    for (ret, img_name) in zip(rets, img_names):
        if ret == True:
            #if output video will be created from alphapose output pictures
#            if (img_name.split(".")[0] + ".png") in os.listdir(image_folder_input_results):
#                image_folder_input = image_folder_input_results
#                img_name = img_name.split(".")[0] + ".png"
#            else:
#                image_folder_input = image_folder_input_real
                
            # do stuff with frame
            res, result_container = detector.face_blur(image_folder_input, folder_output, output_pictures, method, img_name)
            print("res:", "result frame" if res == 1 else "result queue" if res == 2 else "None")
            #print("result container:", len(result_container))
    ##        if res == detector.RESULT_FRAME:
    ##            cv2.imshow("title", result_container)
    ##            cv2.waitKey(0)
    ##            cv2.destroyAllWindows()

            # load processed frame into video
            if res == detector.RESULT_FRAME:
                output_video.write(result_container)
            
            if res == detector.RESULT_QUEUE:
                # print("interpolating: ", detector.face_coordinates_falling, detector.face_coordinates_rising, "delta: ", detector.face_coordinates_delta)
                print("returned cache size ", len(result_container))
                for i in range(0, len(result_container)):
                    output_video.write(result_container.pop())

        else:
            # empty the current detector cache
            for i in range(0, len(detector.frame_cache)):
                output_video.write(detector.frame_cache.pop())
            print("write done")
            break
        
    ##    if res == detector.RESULT_FRAME:
    ##        cv2.imshow("title", frame)
    ##        cv2.waitKey(0)
    ##        cv2.destroyAllWindows()
        
    #empty directory for next video to process
    #delete_files_in_directory(image_folder_input)

    input_video.release()
    output_video.release()
    cv2.destroyAllWindows()
    
    return output_video

                
if __name__ == "__main__":
    
    start_time = time.time()

    #specify detector
    detector = FaceDetector(face_tracking="kalman", max_frame_padding=10)

    #specifiy video path output
    video_path_output = video_path.split("/")
    video_path_output[-1] = "output_" + video_path_output[-1].split(".")[0]
    video_path_output = "/".join(video_path_output) + "_alphapose1.avi"
    
    
    # process frames in video one by one or all at once (faster)
    #method = "frame by frame"
    method = "all frames"

    output_pictures = True
    
    if method == "frame by frame":
        image_folder_input = "alphapose/files_in_process/demo"
        folder_output = "alphapose/files_in_process/results"

        #empty directory just in case
        delete_files_in_directory(image_folder_input)
    
        # process input_video at video_path
        output_video = process_video_frame_by_frame(video_path, video_path_output, image_folder_input, folder_output, output_pictures, detector)
        
    elif method == "all frames":
        image_folder_input = "examples/demo/video_{0}/demo".format(video_path.split("/")[-1].split(".")[0])
        folder_output = "examples/demo/video_{0}/results".format(video_path.split("/")[-1].split(".")[0])

        # create directories if they not already exist
        if not os.path.exists(image_folder_input):
            os.makedirs(image_folder_input)
            
        if not os.path.exists(folder_output):
            os.makedirs(folder_output)

        # specify processor
        processor = AlphaPoseDetector()

        #empty directory just in case
        delete_files_in_directory(image_folder_input)

        # process input_video at video_path
        output_video = process_video_all_frames(video_path, video_path_output, image_folder_input, folder_output, output_pictures, detector, processor)
        


  
end_time = time.time()
print(end_time - start_time)

# play output video
input_video = cv2.VideoCapture(video_path_output)
utls.PlayVideo(input_video, frame_wait_period=60)

input_video.release()
cv2.destroyAllWindows()
