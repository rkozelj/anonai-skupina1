import cv2
import time
import FaceBlurUtils as utls

from FaceDetector import FaceDetector
from paths.paths import model_path, video_path

def process_video(video_path, video_path_output, detector):
    """
    @brief blur faces in video at video_path and returns blurred video at video_path_output
    @param detector: detector used for detecting faces
    """
    print("opening: ", video_path)
    input_video = cv2.VideoCapture(video_path)
    print("Input video:", input_video)

    #default video dimensions
    video_xdim = 800
    video_ydim = 600

    if input_video.isOpened():
        video_xdim = int(input_video.get(3))
        video_ydim = int(input_video.get(4))
        print('video dimensions: ', video_xdim, 'x', video_ydim)
    else:
        print("failed to open video file")
        exit(-1)

    # specify video encoding and format
    fourcc = cv2.VideoWriter_fourcc(*'XVID')

    output_video = cv2.VideoWriter(video_path_output, fourcc, 20.0, (video_xdim, video_ydim))
    print("output to: ", video_path_output)

    cntr = 0
    while(input_video.isOpened()):
        cntr = cntr + 1
        print("\nframe: ", cntr)
        # ret is False if there are no more frames to read
        
        ret, frame = input_video.read()
        #print("ret, frame:", ret, frame)
        
        if ret == True:
            # do stuff with frame
            res, result_container = detector.face_blur(frame) #frame je kot cv2.imread(image_path)
    ##        print("res:", "result frame" if res == 1 else "result queue" if res == 2 else "None")
    ##        print("result container:", len(result_container))
            
    ##        if res == detector.RESULT_FRAME:
    ##            cv2.imshow("title", result_container)
    ##            cv2.waitKey(0)
    ##            cv2.destroyAllWindows()

            # load processed frame into video
            if res == detector.RESULT_FRAME:
                output_video.write(result_container)
            
            if res == detector.RESULT_QUEUE:
                # print("interpolating: ", detector.face_coordinates_falling, detector.face_coordinates_rising, "delta: ", detector.face_coordinates_delta)
                print("returned cache size ", len(result_container))
                for i in range(0, len(result_container)):
                    output_video.write(result_container.pop())
        else:
            # empty the current detector cache
            for i in range(0, len(detector.frame_cache)):
                output_video.write(detector.frame_cache.pop())

            print("write done")
            break
        
    ##    if res == detector.RESULT_FRAME:
    ##        cv2.imshow("title", frame)
    ##        cv2.waitKey(0)
    ##        cv2.destroyAllWindows()

    input_video.release()
    output_video.release()
    cv2.destroyAllWindows()
    
    return output_video

if __name__ == "__main__":

    start_time = time.time()

    # specify detector
    detector = FaceDetector(model_path=model_path, face_tracking="kalman", max_frame_padding=10)

    # specify video path output
    video_path_output = video_path.split("/")
    video_path_output[-1] = "output_" + video_path_output[-1].split(".")[0]
    video_path_output = "/".join(video_path_output) + "_insightface.avi"

    # process input_video at video_path
    output_video = process_video(video_path, video_path_output, detector)

    end_time = time.time()
    print(end_time - start_time)

    # play output video
    input_video = cv2.VideoCapture(video_path_output)
    utls.PlayVideo(input_video,frame_wait_period=60)

    input_video.release()
    cv2.destroyAllWindows()
