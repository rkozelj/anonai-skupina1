## Insightface
Install all packages in requirements.txt.<br/><br/>
#TODO requirements.txt needs to be written.<br/>
#TODO instructions for downloading models and instructions for variable model_path in paths/paths.py<br/> 
#(although folder models is now included in this folder)

Our code can be used for bluring faces on images, videos and video camera. To specify which image or video should be processed go to folder paths and in paths.py 
input image path or video path.<br/><br/>
To blur faces on images use InsightfaceExample.py.<br/>
To blur faces on videos use edit_video.py.<br/>
To blur faces on video camera use webcam_feed.py