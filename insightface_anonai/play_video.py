import cv2
from paths.paths import video_path
input_video = cv2.VideoCapture(video_path)


def PlayVideo(input_video, frame_wait_period=30):
    """
    @brief plays input video, which should already be loaded by opencv
    """
    frame_count = 0

    while input_video.isOpened():
        ret, frame = input_video.read()
        if ret is True:
            frame_count = frame_count + 1
            cv2.imshow('frame', frame)
            if cv2.waitKey(frame_wait_period) & 0xFF == ord('q'):
                break
        else:
            break
    print("end of video frame count:", frame_count)

if __name__ == "__main__":
    input_video = cv2.VideoCapture(video_path)
    PlayVideo(input_video)
    input_video.release()
    cv2.destroyAllWindows()
